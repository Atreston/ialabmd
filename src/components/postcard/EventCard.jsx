import { Link } from 'react-router-dom'
import { CalendarA, ClockA, LocationA, PavelKryutsouCC } from '../../lib/svg'
import Tooltip from '../../lib/tooltip/Tooltip'
import { useGetMarkdown } from '../../hooks/hooks'
import { useEffect, useState } from 'react'
import { parseEvent, trimFileExtension } from '../../lib/text'
import dayjs from 'dayjs'
import 'dayjs/locale/es-mx'
import { fechaEN } from '../../lib/time'

export default function EventCard({ item }) {
	const [loading, setLoading] = useState(true)
	const [markdown, setMarkdown] = useState()
	const [event, setEvent] = useState()

	useGetMarkdown({ setLoading, setMarkdown, fileName: item.name, path: 'eventos'})

	useEffect(() => {
		setEvent(current => {
			if(markdown) return parseEvent(markdown)
		})
	}, [markdown])

	const getDate = (item, format = 'D MMM YYYY') => {
		if(!item) return
		
		const fecha = item.fecha || item.Fecha
		return dayjs(fechaEN(fecha), { locale: 'es-mx' }).format(format)
	}

	return (
		<>
			{loading
				?	<div>Loading</div>
				:	
				<div className='flex w-full xl:w-11/12 rounded-xl shadow-md bg-white dark:bg-slate-900'>
						{/* Left side */}
						<div className='flex flex-col justify-center items-center gap-2 mx-3 p-4 xl:w-1/6'>
							<h2 className='px-2 py-2 rounded-lg text-white bg-slate-600'>
								<span className='block w-10 text-center text-4xl font-bold'>
									{getDate(event, 'D')}
								</span>
							</h2>
							<h3 className='uppercase font-bold text-lg'>
								{getDate(event, 'MMM') || '-'}
							</h3>
						</div>

						{/* Right side */}
						<div className='flex flex-col gap-2 m-3 xl:w-5/6'>
							{/* Title */}
							<h4 className='text-2xl uppercase'><strong>{trimFileExtension(item?.name) || ' '}</strong></h4>
							
							{/* Details */}
							<ul className='flex flex-col sm:flex-row sm:gap-5'>
								<li className='flex gap-2 items-center'>
									<CalendarA className='fill-white w-6 h-6 mb-1' stroke='gray'/> {getDate(event, 'dddd') || '-'}
								</li>
								<li className='flex gap-2 items-center'>
									<ClockA className='w-5 h-5 mb-1'/> {getDate(event, 'h:MM a') || '-'}
								</li>
								<li className='flex gap-2 items-center'>
									<Tooltip content={<PavelKryutsouCC />} direction='left'>
										<Link to={'https://dribbble.com/paulpatap?ref=svgrepo.com'}>
											<LocationA className='w-7 h-7 mb-1' fill='white' stroke='black'/>
										</Link>
									</Tooltip> {event? (event.lugar || event.Lugar) : ' '}
								</li>
							</ul>
							
							{/* Description */}
							<p className='max-h-12 overflow-hidden font-normal text-slate-700 dark:text-slate-300'>
								{event? (event.descripcion || event.Descripcion || event['Descripción']) : ' '}
							</p>
						</div>
					</div>
			}
		</>
	)
}
