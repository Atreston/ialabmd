import { useEffect, useRef, useState } from "react"
import { sortByDate, sortByName } from '../../lib/utils'
import PostList from '../posts/PostList'
import PostCard from "../postcard/PostCard"

const PER_PAGE = 5

export default function Publications({ publications }) {
	const [pageList, setPageList] = useState(publications)
	const infiniteScroll = useRef()

	useEffect(() => {
		if(Array.isArray(publications)) setPageList(publications.slice(0, PER_PAGE))
	}, [publications, setPageList])

	const nextPage = () => {
		setPageList(current => current.concat(
			publications.slice(pageList.length, pageList.length + PER_PAGE)
		))
	}

	const hasMore = () => pageList.length < publications.length

	return (
		<div ref={infiniteScroll} id='infiniteScroll' className='flex flex-col flex-1 mx-auto'>
			<h1 className='text-center text-3xl mx-auto mt-10 mb-8 font-semibold leading-5'>
				Listado de Publicaciones
			</h1>

			<div className='flex gap-3 justify-center md:w-10/12 lg:w-7/12 mx-auto'>
				<div className='inline-block'>
					Ordenar
				</div>

				<button className='bg-slate-500 p-1 rounded-md'
					onClick={() => setPageList(current => sortByName(current))}
				>
					Por nombre
				</button>

				<button className='bg-slate-500 p-1 rounded-md'
					onClick={() => setPageList(current => sortByDate(current))}
				>
					Por fecha
				</button>
			</div>

			<PostList pageList={pageList} hasMore={hasMore} nextPage={nextPage}>
				<PostCard />
			</PostList>
		</div>
	)
}
